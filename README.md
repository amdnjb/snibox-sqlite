# snibox-sqlite


Snibox container working with SQLite3 database for personal use

To build this container use following command:

```bash
git clone https://gitlab.com/pivpav/snibox-sqlite.git
docker build -t snibox ./snibox-sqlite
```

Alternatively you can pull ready to use image from [Docker Hub](https://hub.docker.com/r/snowmean/snibox-sqlite/)

```bash
docker pull snowmean/snibox-sqlite:latest
```


Container runs `rake db:migrate` on every start, in order to create database file if not exist, or update database scheme if required, so backups are highly recommended.

To run this container you might use following command:

```bash
docker run -d --name snibox \
              --volume /path/to/local/db:/app/db/database \
              --publish 80:3000 \
              --restart always \
              snowmean/snibox-sqlite
```

## Environment variables

`DATABASE` - Defines sqlite3 database file location within container.

_Default_: `/app/db/database/snibox.sqlite3`

---

`SECRET_KEY_BASE` - Defines `secret_key_base` parameter for your Rails application. Default one is included into the image already, but general recommendation is to change it.

---

`LOGLEVEL` - Defines logging level for Rails application. Available options are: `debug`, `info`, `warn`, `error` and `fatal`

_Default_: `debug`
